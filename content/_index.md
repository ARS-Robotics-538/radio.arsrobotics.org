---
title: "Home"
slug: 'home'
---
# ARS Radio

## Listen

{{< listen >}}

## Request a song to be played

Disclaimer. The song may not be played immedietely next. It may take a few songs for yours to be played.

{{< request >}}

## Recently played

{{< history >}}

## Request a song to be added.

Can't find the song you want played? We want to allow everybody's music to be represented, within reason
of course. Send us an email at [radio@arsrobotics.org](mailto:radio@arsrobotics.org) and we will see what we can do. As long as
we believe your song to be ok to play in school, we will get the song added to be played.

When writing your email, please set your subject to `Music: <Name of Song/Artist/Album>`, and include:
 
- The name of the song/artist/album you want added.
- The name of the artist (as needed) who performs the song/album. Sometimes multiple artists perform versions of the same song,
or have a different song with the same or similar name. We want to ensure we pick the correct one.
 - (Optional but perferred) A link to the song on Youtube. We currently download most of our songs from Youtube,
 and sending us a link helps us be able to fufill your request faster.

## Want to help us out?

We would love to have your help with ARS Radio. We need people to suggest songs, write announcement scripts, send
news bites, record announcements, and more. If you feel like you can help in any of these rules, please send an
email to [radio@arsrobotics.org](mailto:radio@arsrobotics.org) with a subject line of `Volenteer: <Your Name>`. Please include how you
would like to help.

If you have any feedback for us, please email [radio@arsrobotics.org](mailto:radio@arsrobotics.org) with a subject of `Feedback: <Summary>`.
Please share details of what you would like to be added or improved. Please also include wheither or not you can help
as well.
