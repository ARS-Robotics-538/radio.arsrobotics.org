---
title: "Privacy Policy"
slug: 'privacy-policy'
url: '/privacy-policy'
---

## What we collect

We log IP addresses, Browser user agents, request times, and requested url's.
These are used to reduce malicious activity, and to troubleshoot any issues that
may arise. We currently upload some logs to Grafana Cloud for visualiazation and
monitoring. You can view their privacy policy at https://grafana.com/legal/privacy-policy/.

We also use cookies throughout our various services to track autentication
information, user prefrences, and for anti-forgery purposes. You can read more
about cookies here at https://support.mozilla.org/en-US/kb/cookies-information-websites-store-on-your-computer.
Some browsers allow you to view what cookies are stored, and how they are used.

In addition to cookies, our chat system (Element) uses the local storage api in
your browser to store large amounts of information (such as login tokens, message
cache, encryption keys, and more).

## Who we share your data with. 

We send some traffic through Cloudflare for DDOS protection and DNS management.
Their data collection and management policies are different from ours, and can
be found at https://www.cloudflare.com/privacypolicy/. We also use their analytics
featue on our main site and blog.

We host our main website and blog with Gitlab pages, using Hugo as our static
site generator. You can find their privacy policy at https://about.gitlab.com/privacy/.

We host the rest of our infrastructure (SSO, teamcamp, chat, soical, etc) on Digital Ocean.
Their Privacy Policy is located at https://www.digitalocean.com/legal/privacy-policy, and
their data processing agreement can be viewed at https://www.digitalocean.com/legal/data-processing-agreement.

## How to contact us

If you would like to contact us for any reason, including concerns over your
personal privacy, please use one of the contact methods listed at http://arsrobotics.org/contact-us/. 
